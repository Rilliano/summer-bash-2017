function animateDetails() {
    // Variables
    var jungle = document.querySelector('.jungle');
    var bash = document.querySelector('.bash');
    var glasses = document.querySelector('.glasses');
    var date = document.querySelector('.date');
    var chain = document.querySelector('.chain');

    setTimeout(function() {
        jungle.classList.add('active');
    }, 400);
    setTimeout(function() {
        bash.classList.add('active');
    }, 800);
    setTimeout(function() {
        glasses.classList.add('glasses_animation');
        chain.classList.add('chain_animation');
    }, 1200);
    setTimeout(function() {
        date.classList.add('active');
    }, 2000);
}
