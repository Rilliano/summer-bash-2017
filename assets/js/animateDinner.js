function animateDinner() {
    // Variables
    var dinner = document.querySelector('.dinner');
    var flames = document.querySelector('.flames');

    // Active dinner element based on scroll position
    document.addEventListener('scroll', function() {
      var offSet = dinner.getBoundingClientRect().top;

      if (offSet < 400 && dinner.classList.contains('active') === false) {
        dinner.classList.add('active');
        setTimeout(function() {
            flames.classList.add('flames_animation');
        }, 200);
      }

    //   if (offSet > 400 && dinner.classList.contains('active')) {
    //       dinner.classList.remove('active');
    //       flames.classList.remove('flames_animation');
    //   }
   });
}
