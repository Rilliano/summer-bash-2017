function formValidation() {
    // Variables
    var email = document.querySelector('input[type="email"]');
    var form = document.querySelector('.invite_form');
    var yesButton = document.querySelector('.yes');
    var noButton = document.querySelector('.no');
    var yesVideo = document.querySelector('.yes_video video');
    var noVideo = document.querySelector('.no_video video');
    var audio = document.querySelector('audio');

    yesButton.addEventListener('click', function() {
        if (email.checkValidity() === true) {
            this.setAttribute('clicked', 'true');
        }
    });

    noButton.addEventListener('click', function() {
        if (email.checkValidity() === true) {
            this.setAttribute('clicked', 'true');
        }
    });

    form.addEventListener('submit', function(e) {
        e.preventDefault();
        var clickedButton = document.querySelector('input[type=submit][clicked=true]');
        var value = clickedButton.value;

        if (value === 'yes') {
            yesVideo.parentElement.classList.add('active');
            yesVideo.play();
            yesVideo.loop = true;
            audio.pause();
            document.body.style.overflow = 'hidden';
        } else if (value === 'no') {
            noVideo.parentElement.classList.add('active');
            noVideo.play();
            noVideo.loop = true;
            audio.pause();
            document.body.style.overflow = 'hidden';
        }
        // clickedButton.removeAttribute('clicked');
    });
}
