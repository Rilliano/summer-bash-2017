function animateSlide() {
    // Variables
    var slide = document.querySelector('.slide');
    var elephant = document.querySelector('.elephant');
    var ball = document.querySelector('.ball');
    var waterSplash = document.querySelector('.water_splash');

    // Active slide element based on scroll position
    document.addEventListener('scroll', function() {
      var offSet = slide.getBoundingClientRect().top;

      if (offSet < 400 && slide.classList.contains('active') === false) {
        slide.classList.add('active');
        setTimeout(function() {
            ball.classList.add('active');
        }, 500);
        setTimeout(function() {
            ball.classList.add('ball_bounce_animation');
        }, 800);
        setTimeout(function() {
            waterSplash.classList.add('water_splash_animation');
        }, 1000);
      }

    //   if (offSet > 400 && slide.classList.contains('active')) {
    //       slide.classList.remove('active');
    //       ball.classList.remove('active');
    //       ball.classList.remove('ball_bounce_animation');
    //       waterSplash.classList.remove('water_splash_animation');
    //   }
   });

   // Animate the elephant
   setInterval(function() {
       elephant.classList.add('elephant_animation');
       setTimeout(function() {
           elephant.classList.remove('elephant_animation');
       }, 1000);
   }, 4000);
}
