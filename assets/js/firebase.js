"use strict";

var ref = new Firebase("https://summer-bash-2017.firebaseio.com/");

if (document.querySelector('.invite_form')) {
    var form = document.querySelector('.invite_form');

    form.addEventListener('submit', postComment);

    var timeStamp = function timeStamp() {
        var options = {
            month: '2-digit',
            day: '2-digit',
            year: '2-digit',
            hour: '2-digit',
            minute: '2-digit'
        };
        var now = new Date().toLocaleString('en-US', options);
        return now;
    };

    function postComment() {
        var email = document.querySelector('input[type="email"]').value;
        var name = document.querySelector('input[type="text"]').value;
        var attendingValue = document.querySelector('input[type=submit][clicked=true]').value;

        if (email) {
            ref.push({
                name: name,
                email: email,
                attending: attendingValue,
                time: timeStamp()
            });
        }
    };
}

if (document.getElementById('attending_list')) {
    ref.on('child_added', function (snapshot) {
        var comment = snapshot.val();
        addComment(comment.name, comment.email, comment.attending, comment.time);
    });

    var addComment = function addComment(name, email, attending, timeStamp) {
        var attendingList = document.getElementById('attending_list');
        attendingList.innerHTML = '<hr>Name: ' + name + '<br> Email: ' + email + '<br>Attending: ' + attending + '<br>Time: '+ timeStamp + attendingList.innerHTML;
    };
}
