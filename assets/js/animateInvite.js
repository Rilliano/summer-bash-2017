function animateInvite() {
    // Variables
    var invite = document.querySelector('.invite');
    var pattern = document.querySelector('.invite_pattern');
    var background = document.querySelector('.invite_bg');
    var title = document.querySelector('.invite_title');
    var form = document.querySelector('.invite_form');

    // Active invite element based on scroll position
    document.addEventListener('scroll', function() {
        var offSet = invite.getBoundingClientRect().top;

        if (offSet < 400 && invite.classList.contains('active') === false) {
            invite.classList.add('active');
            pattern.classList.add('active');
            setTimeout(function() {
                background.classList.add('active');
            }, 200);
            setTimeout(function() {
                title.classList.add('active');
            }, 500);
            setTimeout(function() {
                form.classList.add('active');
            }, 700);
        }

        // if (offSet > 400 && invite.classList.contains('active')) {
        //     invite.classList.remove('active');
        //     pattern.classList.remove('active');
        //     background.classList.remove('active');
        //     title.classList.remove('active');
        //     form.classList.remove('active');
        // }
    });
}
