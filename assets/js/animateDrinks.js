function animateDrinks() {
    // Variables
    var drinks = document.querySelector('.drinks');
    var indianPattern = document.querySelector('.indian_pattern');
    var bottleHandsRight = document.querySelector('.bottle_hands_right');
    var bottleHandsLeft = document.querySelector('.bottle_hands_left');
    var shakeDrink = document.querySelector('.shake_drink');

    // Active drinks element based on scroll position
    document.addEventListener('scroll', function() {
      var offSet = drinks.getBoundingClientRect().top;

      if (offSet < 400 && drinks.classList.contains('active') === false) {
        indianPattern.classList.add('active');
        setTimeout(function() {
            drinks.classList.add('active');
        }, 500);
        setTimeout(function() {
            shakeDrink.classList.add('shake_drink_animation');
        }, 1100);
        setTimeout(function() {
            bottleHandsRight.classList.add('bottle_hands_right_animation');
            bottleHandsLeft.classList.add('bottle_hands_left_animation');
        }, 1600);
      }

    //   if (offSet > 400 && drinks.classList.contains('active')) {
    //       indianPattern.classList.remove('active');
    //       drinks.classList.remove('active');
    //       shakeDrink.classList.remove('shake_drink_animation');
    //       bottleHandsRight.classList.remove('bottle_hands_right_animation');
    //       bottleHandsLeft.classList.remove('bottle_hands_left_animation');
    //   }
   });
}
