
window.onload = function(){

    var indiaSound = document.querySelector('.india-sound');
    indiaSound.autoplay = true
    indiaSound.load();

    document.querySelector('body').classList.add('pageLoaded');
    document.querySelector('body').classList.remove('pageLoading');

    setTimeout(function() {
        animateDetails();
        animateDrinks();
        animateDinner();
        animateSlide();
        animateInvite();
        formValidation();
    }, 600);

};
